package Test;

import Core.Field;
import Testing.Disparable;
import Core.PhysicBody;
import Core.Sprite;

public class Projectil extends PhysicBody implements Constants {

	public Projectil(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.trigger = true;
	}
	
	public Projectil(Projectil p, int x1, int y1, int x2, int y2){
		super(p.name, x1, y1, x2, y2, p.angle, p.path, p.f);
		this.trigger = true;
	}
	
	public void move(){	
		this.setVelocity(4,0);
	}

@Override
public void update() {
	if (this.x1 > XMAX ) this.delete();
}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		//System.out.println("Colisi� amb " + sprite.name);
		if (sprite instanceof Disparable) {
			Disparable d = (Disparable) sprite;
			d.danyar();
			System.out.println("pum");
			//sprite.delete();
			this.delete();
		}
		
	}


	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

}
