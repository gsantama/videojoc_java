package Test;

import java.util.Set;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Personatge_4c extends PhysicBody {

	public boolean aterra;
	private boolean doble;
	
	public Personatge_4c(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		doble = false;
		aterra = false;
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		aterra = true;
		doble = false;
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		aterra = false;		
	}

	@Override
	public void onCollisionStay(Sprite sprite) {		
	}

	public void move(Set<Character> keys) {
		if(keys.contains('d')) {
			this.setVelocity(2, this.velocity[1]);
			flippedX=true;
		}else if(keys.contains('a')){
			this.setVelocity(-2, this.velocity[1]);
			flippedX=false;
		}else {
			this.setVelocity(0, this.velocity[1]);
		}

	}

	public void jump() {
		if (this.aterra) {
			this.addForce(0, -2);
			doble = true;
		}
		if (!this.aterra && this.doble) {
			this.addForce(0, -2);
			doble = false;
		}
	}


	
}
