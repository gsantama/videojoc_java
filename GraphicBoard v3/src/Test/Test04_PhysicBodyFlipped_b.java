package Test;

import java.util.Set;

import Core.Field;
import Core.Window;


public class Test04_PhysicBodyFlipped_b {
	static Field f = new Field();
	static Window w = new Window(f, 700, 500);

	public static void main(String[] args) throws InterruptedException {
		Roca terra = new Roca("Terra", 50, 300, 2500, 350, 0, "resources\\swap.png", f, 0);
		Roca plataforma = new Roca("Plataforma", 500, 180, 600, 190, 0, "resources\\swap.png", f, 0);
		Personatge p1 = new Personatge("link", 50, 250, 100, 300, 0, "resources/link1.gif", f);
		p1.flippedX=false;
		p1.setConstantForce(0, 0.2);

		while (true) {
			f.draw();
			Thread.sleep(30);
			Set<Character> keys = input();
			p1.move(keys);
			if (w.getKeysDown().contains('w') && p1.aterra) {
					p1.jump();
			}
		}
	}

	private static Set<Character> input() {
		// TODO Auto-generated method stub
		return w.getPressedKeys();
	}

}
