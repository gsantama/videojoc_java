package Test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import Testing.Disparable;

public class Malote extends PhysicBody implements Constants, Disparable, Serializable{
	int vida;
	public Malote(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		vida = 3;
	}

	public int treuVida() {
		if (vida > 0) vida --;
		return vida;
	}
	
	public int getVida() {
		return vida;
	}
	
	@Override
	public void onCollisionEnter(Sprite sprite) {
		if (sprite instanceof Personatge_5) {
			System.out.println("TOUCH�");
		}
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
	}

	@Override
	public void onCollisionStay(Sprite sprite) {		
	}

	public void move(Sprite p1) {
		int pxcenter = (p1.x1+p1.x2)/2;
		int pycenter = (p1.y1+p1.y2)/2;
		int fxcenter = (x1+x2)/2;
		int fycenter = (y1+y2)/2;
		if(pxcenter<fxcenter) 
			this.setVelocity(-1.5, this.velocity[1]);
		else
			this.setVelocity(1.5, this.velocity[1]);
	
		if(pycenter<fycenter) 
			this.setVelocity(this.velocity[0], -1.5 );
		else
			this.setVelocity(this.velocity[0], 1.5 );
	}

	@Override
	public void danyar() {
		this.vida --;
		Test06_b.estat = EstatJoc.GUANYA;
		if (vida == 0) this.delete();
	}		
}
