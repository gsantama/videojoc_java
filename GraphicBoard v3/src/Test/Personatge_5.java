package Test;

import java.util.ArrayList;
import java.util.Set;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;


public class Personatge_5 extends PhysicBody implements Constants{

	public boolean aterra;
	private boolean doble;
	public Projectil projectil;
	
	public Personatge_5(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		doble = false;
		aterra = false;
		this.projectil = null;
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		aterra = true;
		doble = false;
	
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		aterra = false;		
	}

	@Override
	public void onCollisionStay(Sprite sprite) {		
	}

	public void move(Set<Character> keys) {
		if(keys.contains('d')) {
			this.setVelocity(2, this.velocity[1]);
			flippedX=true;
		}else if(keys.contains('a')){
			this.setVelocity(-2, this.velocity[1]);
			flippedX=false;
		}else if (keys.contains('z')) {
			this.disparar();
		}else {
			//this.setVelocity(0, this.velocity[1]);
		}
		if (this.x1 < XMIN) {
			this.x1 = XMIN;
			this.x2 = MIDAX;
		}
		if (this.x2 > (XMAX - MIDAX)) {
			this.x1 = XMAX - 2*MIDAX;
			this.x2 = XMAX - MIDAX;
		}
		if (this.y2 > YMAX) {
			this.y1 = YMAX - MIDAY;
			this.y2 = YMAX;
		}
		if (this.y1 < YMIN) {
			this.y1 = YMIN;
			this.y2 = YMIN + MIDAY;
		}
	}

	public void disparar() {
		this.projectil = new Projectil("mandarina", this.x1+60, this.y1, this.x2+60, this.y2, this.angle, "resources\\mandarina.png", this.f);
		this.projectil.move();
	}
	
	public void jump() {
		if (this.aterra) {
			this.addForce(0, -2);
			doble = true;
		}else if (this.doble) {
			this.addForce(0, -2);
			doble = false;
		}
	}	
}
