package Test;

import Core.Field;
import Core.Sprite;
import Core.Window;

public class Test03 {
	static Field f = new Field();
	static Window w = new Window(f);
	static RocaControlable roca2 = new RocaControlable("Roca", 150, 50, 200, 100, 0, "resources\\b84.png", f, 0);

	public static void main(String[] args) throws InterruptedException {

		// Roca roca1 = new Roca("Roca", 50,50,100,100,0,"resources\\rock1.png",f);
//		Roca roca2 = new Roca("Roca", 150,50,200,100,0,"resources\\b84.png",f);
		for (;;) {
			input();
			f.draw();
			Thread.sleep(100);
		}
	}

	public static void input() {
		if (w.getPressedKeys().contains('w'))
			roca2.input (Input.AMUNT);
		
		if (w.getPressedKeys().contains('s'))
			roca2.input (Input.AVALL);
		
		if (w.getPressedKeys().contains('a'))
			roca2.input (Input.ESQUERRA);
		
		if (w.getPressedKeys().contains('d'))
			roca2.input (Input.DRETA);
		
//		System.out.println("Tecles: " + w.getPressedKeys());   //si vols veure les tecles polsades
		}
}
