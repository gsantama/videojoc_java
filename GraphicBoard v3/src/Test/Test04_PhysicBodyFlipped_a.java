package Test;

import java.util.Set;

import Core.Field;
import Core.Sprite;
import Core.Window;

public class Test04_PhysicBodyFlipped_a {
	static Field f = new Field();
	static Window w = new Window(f, 700, 500);

	public static void main(String[] args) throws InterruptedException {
		Roca plataforma = new Roca("Terra", 50, 400, 650, 450, 0, "resources\\swap.png", f, 0);
		Personatge p1 = new Personatge("link", 50, 350, 100, 400, 0, "resources/link1.gif", f);
		p1.flippedX=false;
		p1.setConstantForce(0, 0.2);
		while (true) {
			f.draw();
			Thread.sleep(30);
			Set<Character> keys = input();
			p1.move(keys);
			if (w.getKeysDown().contains('w')) {
				p1.jump();
			}
		}
	}

	private static Set<Character> input() {
		// TODO Auto-generated method stub
		return w.getPressedKeys();
	}

}
