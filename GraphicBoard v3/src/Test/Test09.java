package Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Set;

import Core.Field;
import Core.Sprite;
import Core.Window;


public class Test09 implements Constants{
	static Field f = new Field();
	static Window w = new Window(f, 1600, 880);
	static final String fitxer = "src\\files\\game.dat";
	static PersonatgeFile p1 = new PersonatgeFile("link", 50, 250, 50 + MIDAX, 250 + MIDAY, 0, "resources/link1.gif", f);
	
	public static void main(String[] args) throws InterruptedException {
		Roca terra = new Roca("Terra", 0, 800, 1600, 820, 0, "resources\\swap.png", f, 0);
		
		p1.setConstantForce(0, 0.2);

		ArrayList<Texto> textos = new ArrayList<Texto>();
		textos.add(new Texto("vides", 20, 20, 60, 60, "", f));
		textos.add(new Texto("projectils", 20, 60, 60, 120, "", f));
		textos.add(new Texto("posicio", 20, 120, 60, 180, "", f));
		
		while (true) {
			texto_update(textos, p1);
			f.draw();
			Thread.sleep(20);
			Set<Character> keys = input();
			p1.move(keys);
			Set<Character> keysd = inputd();
			
			if (keysd.contains(' '))
				p1.disparar();
			if (keysd.contains('w'))
				p1.jump();
			
			if (keysd.contains('b'))
				backup();		//backup del joc a fitxer  files/game.dat
			if (keysd.contains('r')) {
				restore();		// restore del joc del fitxer files/game.dat
			}
		}		
	}

	private static Set<Character> input() {
		return w.getPressedKeys();
	}

	private static Set<Character> inputd() {
		return w.getKeysDown();
	}
	
	public static void texto_update(ArrayList<Texto> textos, PersonatgeFile p) {
		textos.get(0).path = "Vides: " + p.vides;
		textos.get(1).path = "Projectils: " + p.projectils;
		textos.get(2).path = "Coordenades: (" +  p.x1 + "," + p.y1 + ") ("+ p.x2 + "," + p.y2 + ")";
	}
	public static void backup() {
		
		try {
			File f = new File(fitxer);
			//funciona de forma similar a un fileReader
			FileOutputStream fos = new FileOutputStream(f);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			oos.writeObject(p1);
			oos.flush();
			oos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Game saved");
	}
	
	public static void restore() {
		try {
			PersonatgeFile p = new PersonatgeFile("", 0, 0, 0, 0, 0, "", f);
			File fic = new File(fitxer);
			//funciona de forma similar a un fileReader
			FileInputStream fis = new FileInputStream(fic);
			ObjectInputStream ois = new ObjectInputStream(fis);
					
			
			Object o = ois.readObject();
			if(o instanceof PersonatgeFile) {
				p  = (PersonatgeFile) o;
				p1.Reload(p);
				System.out.println("Recuperat Personatge");
				System.out.println( "Vides: " + p1.vides);
				System.out.println("Projectils: " + p1.projectils);
				System.out.println("Coordenades: (" +  p1.x1 + "," + p1.y1 + ") ("+ p1.x2 + "," + p1.y2 + ")");
			}			
			
		} catch (FileNotFoundException e) {
			System.out.println("no existeix el fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("excepció d'entrada/sortida");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("no s'ha trobat la classe demanada");
			e.printStackTrace();
		}
		}


}
