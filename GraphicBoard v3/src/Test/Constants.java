package Test;

public interface Constants {
	final int GAP = 10;
	final int XMIN = 0;
	final int XMAX = 1600;
	final int YMIN = 0;
	final int YMAX = 800;
	
	final int MIDAX = 50;
	final int MIDAY = 50;
}
