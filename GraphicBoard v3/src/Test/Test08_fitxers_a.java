package Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Set;

import Core.Field;
import Core.Sprite;
import Core.Window;


public class Test08_fitxers_a implements Constants{
	static Field f = new Field();
	static Window w = new Window(f, 1600, 880);
	static Test.EstatJoc estat;
	static Partida laMevaPartida;
	
	public static void main(String[] args) throws InterruptedException {
		Roca terra = new Roca("Terra", 0, 800, 1600, 820, 0, "resources\\swap.png", f, 0);
		Roca plataforma1 = new Roca("Plataforma", 500, 680, 600, 690, 0, "resources\\swap.png", f, 0);
		Roca plataforma2 = new Roca("Plataforma", 300, 520, 400, 530, 0, "resources\\swap.png", f, 0);
		Roca plataforma3 = new Roca("Plataforma", 500, 420, 600, 430, 0, "resources\\swap.png", f, 0);
		Obstacle o1 = new Obstacle("o1", 700,400,705,YMAX,0,"resources\\swap.png",f);
		Obstacle o2 = new Obstacle("o2", 1000,400,1005,YMAX,0,"resources\\swap.png",f);
		Obstacle o3 = new Obstacle("o2", 705,400,1000,405,0,"resources\\swap.png",f);
		Obstacle o4 = new Obstacle("o2", 705,600,1000,605,0,"resources\\swap.png",f);
		laMevaPartida = new Partida();
		
		Personatge_6 p1 = new Personatge_6("link", 50, 250, 50 + MIDAX, 250 + MIDAY, 0, "resources/link1.gif", f);
		Malote papyrus = new Malote("muMalo", XMAX - 2*MIDAX, 0, XMAX- MIDAX, MIDAY, 0, "resources/papyrus.png", f);
		p1.flippedX=false;
		p1.setConstantForce(0, 0.2);

		laMevaPartida.personatge = p1;
		laMevaPartida.malote = papyrus;
		
		estat = EstatJoc.ENJOC;
		while (estat == EstatJoc.ENJOC) {
			f.draw();
			Thread.sleep(20);
			Set<Character> keys = input();
			p1.move(keys);
			Set<Character> keysd = inputd();
			
			if (keysd.contains(' '))
				p1.disparar();
			if (keysd.contains('w'))
				p1.jump();
			papyrus.move(p1);
			
			if (keysd.contains('b'))
				backup();		//backup del joc a fitxer  files/game.dat
			if (keysd.contains('r'))
				restore();		// restore del joc del fitxer files/game.dat
		}
		
		switch(estat) {
		case MORT:	w.showPopup("Has perdut");break;
		case GUANYA: w.showPopup("Has guanyat");break;
		}
	}

	private static Set<Character> input() {
		return w.getPressedKeys();
	}

	private static Set<Character> inputd() {
		return w.getKeysDown();
	}
	
	public static void backup() {
		
		try {
			File f = new File("src\\files\\game.dat");
			//funciona de forma similar a un fileReader
			FileOutputStream fos = new FileOutputStream(f);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			oos.writeObject(laMevaPartida.personatge.x1);
			oos.writeObject(laMevaPartida.personatge.y1);
			oos.writeObject(laMevaPartida.personatge.x2);
			oos.writeObject(laMevaPartida.personatge.y2);
			oos.writeObject(laMevaPartida.malote.x1);
			oos.writeObject(laMevaPartida.malote.y1);
			oos.writeObject(laMevaPartida.malote.x2);
			oos.writeObject(laMevaPartida.malote.y2);
			oos.flush();
			oos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Game saved");
	}
	
	public static void restore() {
		//laMevaPartida.personatge =
		//laMevaPartida.malote =
		try {
			File f = new File("src\\files\\game.dat");
			//funciona de forma similar a un fileReader
			FileInputStream fis = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fis);
					
			
			Object o = ois.readObject();
			if(o instanceof Integer) {
				laMevaPartida.personatge.x1  = (Integer) o;
			}
	
			o = ois.readObject();
			if(o instanceof Integer) {
				laMevaPartida.personatge.y1  = (Integer) o;
			}
			o = ois.readObject();
			if(o instanceof Integer) {
				laMevaPartida.personatge.x2  = (Integer) o;
			}
			o = ois.readObject();
			if(o instanceof Integer) {
				laMevaPartida.personatge.y2  = (Integer) o;
			}


			o = ois.readObject();
			if(o instanceof Integer) {
				laMevaPartida.malote.x1  = (Integer) o;
			}
	
			o = ois.readObject();
			if(o instanceof Integer) {
				laMevaPartida.malote.y1  = (Integer) o;
			}
			o = ois.readObject();
			if(o instanceof Integer) {
				laMevaPartida.malote.x2  = (Integer) o;
			}
			o = ois.readObject();
			if(o instanceof Integer) {
				laMevaPartida.malote.y2  = (Integer) o;
			}

			
			
		} catch (FileNotFoundException e) {
			System.out.println("no existeix el fitxer");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("excepció d'entrada/sortida");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("no s'ha trobat la classe demanada");
			e.printStackTrace();
		}
	}


}
