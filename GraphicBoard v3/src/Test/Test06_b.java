package Test;

import java.util.Set;

import Core.Field;
import Core.Sprite;
import Core.Window;


public class Test06_b implements Constants{
	static Field f = new Field();
	static Window w = new Window(f, 1600, 880);
	static Test.EstatJoc estat;
	
	public static void main(String[] args) throws InterruptedException {
		Roca terra = new Roca("Terra", 0, 800, 1600, 820, 0, "resources\\swap.png", f, 0);
		Roca plataforma1 = new Roca("Plataforma", 500, 680, 600, 690, 0, "resources\\swap.png", f, 0);
		Roca plataforma2 = new Roca("Plataforma", 300, 520, 400, 530, 0, "resources\\swap.png", f, 0);
		Roca plataforma3 = new Roca("Plataforma", 500, 420, 600, 430, 0, "resources\\swap.png", f, 0);
		Obstacle o1 = new Obstacle("o1", 700,400,705,YMAX,0,"resources\\swap.png",f);
		Obstacle o2 = new Obstacle("o2", 1000,400,1005,YMAX,0,"resources\\swap.png",f);
		Obstacle o3 = new Obstacle("o2", 705,400,1000,405,0,"resources\\swap.png",f);
		Obstacle o4 = new Obstacle("o2", 705,600,1000,605,0,"resources\\swap.png",f);

		Personatge_6 p1 = new Personatge_6("link", 50, 250, 50 + MIDAX, 250 + MIDAY, 0, "resources/link1.gif", f);
		Malote papyrus = new Malote("muMalo", XMAX - 2*MIDAX, 0, XMAX- MIDAX, MIDAY, 0, "resources/papyrus.png", f);
		p1.flippedX=false;
		p1.setConstantForce(0, 0.2);
		estat = EstatJoc.ENJOC;
		while (estat == EstatJoc.ENJOC) {
			f.draw();
			Thread.sleep(20);
			Set<Character> keys = input();
			p1.move(keys);
			Set<Character> keysd = inputd();
			
			if (keysd.contains(' '))
				p1.disparar();
			if (keysd.contains('w'))
				p1.jump();
			papyrus.move(p1);
		}
		
		switch(estat) {
		case MORT:	w.showPopup("Has perdut");break;
		case GUANYA: w.showPopup("Has guanyat");break;
		}
	}

	private static Set<Character> input() {
		return w.getPressedKeys();
	}

	private static Set<Character> inputd() {
		return w.getKeysDown();
	}
}
