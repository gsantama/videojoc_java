package Ronin;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class enemy extends PhysicBody{
	
	public boolean direction = true;
	public Direction vision;
	public boolean detect = false;
	public int startPosX;
	public int startPosY;

	public enemy(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.startPosX = x1;
		this.startPosY = y1;
	}
	
	public void automoveX (int a, int b, player pla){
		if(this.x2 < a && this.direction == true){
			this.x1+=3;
			this.x2+=3;
			this.vision = Direction.RIGHT;
		}
		else if(this.x2 >= a){
			this.direction = false;
			this.x1-=3;
			this.x2-=3;
			this.vision = Direction.LEFT;
		}
		else if(this.x2 > b && this.direction == false){
			this.x1-=3;
			this.x2-=3;
			this.vision = Direction.LEFT;
		}
		else if(this.x2 <= b){
			this.direction = true;
			this.x1+=3;
			this.x2+=3;
			this.vision = Direction.RIGHT;
		}
		watching(pla);
	}
	public void automoveY (int a, int b, player pla){
		if(this.y2 < a && this.direction == true){
			this.y1+=3;
			this.y2+=3;
			this.vision = Direction.DOWN;
		}
		else if(this.y2 >= a){
			this.direction = false;
			this.y1-=3;
			this.y2-=3;
			this.vision = Direction.UP;
		}
		else if(this.y2 > b && this.direction == false){
			this.y1-=3;
			this.y2-=3;
			this.vision = Direction.UP;
		}
		else if(this.y2 <= b){
			this.direction = true;
			this.y1+=3;
			this.y2+=3;
			this.vision = Direction.DOWN;
		}
		watching(pla);
	}
	public boolean watching(player pla){
		if(pla.x2 > (this.x1-200) && pla.x2 < this.x1 && this.vision.equals(Direction.LEFT) && pla.y1 < (this.y2+50) && pla.y2 > (this.y1-50)){
			return this.detect = true;
		}
		else if(pla.x1 < (this.x2+200) && pla.x1 > this.x2 && this.vision.equals(Direction.RIGHT) && pla.y1 < (this.y2+50) && pla.y2 > (this.y1-50)){
			return this.detect = true;
		}
		else if(pla.y1 < (this.y2+200) && pla.y1 > this.y2 && this.vision.equals(Direction.DOWN) && pla.x2 > (this.x1-50) && pla.x1 < (this.x2+50)){
			return this.detect = true;
		}
		else if(pla.y2 > (this.y1-200) && pla.y2 < this.y1 && this.vision.equals(Direction.UP) && pla.x2 > (this.x1-50) && pla.x1 < (this.x2+50)){
			return this.detect = true;
		}
		else{
			return this.detect = false;
		}
	}
	public void pursuit (player pla){
		if (pla.x2 > (this.x1+400) || pla.x1 < (this.x2-400) || pla.y1 > (this.y2+400) || pla.y2 < (this.y1-400)){
			Return();
		}
		else if(pla.x2 < this.x1){
			this.x1-=3;
			this.x2-=3;
		}
		else if(pla.x1 > this.x2){
			this.x1+=3;
			this.x2+=3;
		}
		else if(pla.y1 > this.y2){
			this.y1+=3;
			this.y2+=3;
		}
		else if(pla.y2 < this.y1){
			this.y1-=3;
			this.y2-=3;
		}
		else if(pla.x2 < this.x1 && pla.y1 > this.y2){
			this.x1-=3;
			this.x2-=3;
			this.y1+=3;
			this.y2+=3;
		}
		else if(pla.x2 < this.x1 && pla.y2 < this.y1){
			this.x1-=3;
			this.x2-=3;
			this.y1+=3;
			this.y2+=3;
		}
		else if(pla.x1 > this.x2 && pla.y1 > this.y2){
			this.x1+=3;
			this.x2+=3;
			this.y1+=3;
			this.y2+=3;
		}
		else if(pla.x1 > this.x2 && pla.y2 < this.y1){
			this.x1+=3;
			this.x2+=3;
			this.y1+=3;
			this.y2+=3;
		}
	}
	public void Return (){
		if(this.startPosX < this.x1){
			this.x1-=3;
			this.x2-=3;
		}
		else if(this.startPosX > this.x1){
			this.x1+=3;
			this.x2+=3;
		}
		else if(this.startPosY > this.y1){
			this.y1+=3;
			this.y2+=3;
		}
		else if(this.startPosY < this.y1){
			this.y1-=3;
			this.y2-=3;
		}
		else if(this.startPosX < this.x1 && this.startPosY > this.y1){
			this.x1-=3;
			this.x2-=3;
			this.y1+=3;
			this.y2+=3;
		}
		else if(this.startPosX < this.x1 && this.startPosY < this.y1){
			this.x1-=3;
			this.x2-=3;
			this.y1+=3;
			this.y2+=3;
		}
		else if(this.startPosX > this.x1 && this.startPosY > this.y1){
			this.x1+=3;
			this.x2+=3;
			this.y1+=3;
			this.y2+=3;
		}
		else if(this.startPosX > this.x1 && this.startPosY < this.y1){
			this.x1+=3;
			this.x2+=3;
			this.y1+=3;
			this.y2+=3;
		}
		else{
			this.detect = false;
		}
	}
	public void IAY (int a, int b, player pla){
		if(this.detect == true){
			this.pursuit(pla);
		}
		else{
			this.automoveY(a, b, pla);
		}
	}
	public void IAX (int a, int b, player pla){
		if(this.detect == true){
			this.pursuit(pla);
		}
		else{
			this.automoveX(a, b, pla);
		}
	}
	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

}
