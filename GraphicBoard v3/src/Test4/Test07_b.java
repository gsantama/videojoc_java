package Test4;

import java.util.Set;

import Core.Field;
import Core.Sprite;
import Core.Window;


public class Test07_b implements Constants{
	static Field f = new Field();
	static Window w = new Window(f, 1600, 880);
	static Test.EstatJoc estat;
	
	public static void main(String[] args) throws InterruptedException {

		Nau p1 = new Nau("nau", (XMAX - MIDAX)/2, (YMAX - MIDAY), (XMAX + MIDAX)/2, YMAX, 0, "resources/vulnerary.png", f);
		int nEnemic=0;
		int cicle = 0;
		
		while (true) {
			if (cicle%CICLES == 0) {
				int ex = (int) (Math.random()*XMAX);
				Enemic e = new Enemic("malote" + (++nEnemic),ex,0,ex + MIDAX, MIDAY,0,"resources/ganso.png",f);
			};
			cicle++;
			f.draw();
			Thread.sleep(20);
			Set<Character> keys = input();
			p1.move(keys);
			Set<Character> keysd = inputd();
			
			if (keysd.contains(' '))
				p1.disparar();
		}
			
		
	}

	private static Set<Character> input() {
		return w.getPressedKeys();
	}

	private static Set<Character> inputd() {
		return w.getKeysDown();
	}
}
