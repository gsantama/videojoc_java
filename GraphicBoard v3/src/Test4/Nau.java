package Test4;

import java.util.ArrayList;
import java.util.Set;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;


public class Nau extends PhysicBody implements Constants{

	public Nau(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
	
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
	}

	@Override
	public void onCollisionStay(Sprite sprite) {		
	}

	public void move(Set<Character> keys) {
		if(keys.contains('d')) {
			this.setVelocity(2, 0);
			flippedX=true;
		}else if(keys.contains('a')){
			this.setVelocity(-2, 0);
			flippedX=false;
		}else {
			//this.setVelocity(0, 0);
		}
		if (this.x1 < XMIN) {
			this.x1 = XMIN;
			this.x2 = MIDAX;
		}
		if (this.x2 > (XMAX - MIDAX)) {
			this.x1 = XMAX - MIDAX*2;
			this.x2 = XMAX-MIDAX;
		}
	}

	public void disparar() {
		Projectil p = new Projectil("balazo", (this.x1 + this.x2)/2 -1, this.y1-15, (this.x1 + this.x2)/2 + 1, this.y1, this.angle, "resources\\rainb.jpg", this.f);
		p.move();
	}
}
