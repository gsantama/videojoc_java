package Test4;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class EnemicXungo extends Enemic {
	public EnemicXungo(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.setVelocity(0, 1 );
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		if (sprite instanceof Nau) {
			sprite.delete();
			this.delete();
			System.out.println("MHAN MATAO");
			System.exit(0);	//provisional
		}		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
	}

	@Override
	public void onCollisionStay(Sprite sprite) {		
	}
	
	public void disparar(Nau n) {
		Projectil p = new Projectil("bomba", (this.x1 + this.x2)/2 -1, this.y1-15, (this.x1 + this.x2)/2 + 1, this.y1, this.angle, "resources\\rainb.jpg", this.f);
		p.setVelocity(0,3);
		if (n.x1 < this.x1) {	
			this.x1 =- GAP; 
			this.x2 =- GAP;
		}else if(n.x1> this.x2) {
			this.x1 =+ GAP; 
			this.x2 =+ GAP;			
		}
	}	
}
