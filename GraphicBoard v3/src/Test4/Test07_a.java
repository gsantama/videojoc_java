package Test4;

import java.util.Set;

import Core.Field;
import Core.Sprite;
import Core.Window;


public class Test07_a implements Constants{
	static Field f = new Field();
	static Window w = new Window(f, 1600, 880);
	static Test.EstatJoc estat;
	
	public static void main(String[] args) throws InterruptedException {

		Nau p1 = new Nau("link", (XMAX - MIDAX)/2, (YMAX - MIDAY), (XMAX + MIDAX)/2, YMAX, 0, "resources/vulnerary.png", f);
//		Malote papyrus = new Malote("muMalo", XMAX - 2*MIDAX, 0, XMAX- MIDAX, MIDAY, 0, "resources/papyrus.png", f);
		p1.flippedX=false;
		//p1.setConstantForce(0, 0.2);
		while (true) {
			f.draw();
			Thread.sleep(20);
			Set<Character> keys = input();
			p1.move(keys);
			Set<Character> keysd = inputd();
			
			if (keysd.contains(' '))
				p1.disparar();
		}
			
		
	}

	private static Set<Character> input() {
		return w.getPressedKeys();
	}

	private static Set<Character> inputd() {
		return w.getKeysDown();
	}
}
