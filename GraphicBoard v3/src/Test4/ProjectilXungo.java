package Test4;

import Core.Field;
import Testing.Disparable;
import Core.PhysicBody;
import Core.Sprite;

public class ProjectilXungo extends Projectil  {

	public ProjectilXungo(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.trigger = true;
	}
	
	public ProjectilXungo(ProjectilXungo p, int x1, int y1, int x2, int y2){
		super(p.name, x1, y1, x2, y2, p.angle, p.path, p.f);
		this.trigger = true;
	}
	
	public void move(){	
		this.setVelocity(0,3);
	}

@Override
public void update() {
	if (this.y1 > YMAX ) this.delete();
}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		if (sprite instanceof Nau) {
			sprite.delete();
			System.out.println("MAN MATTAAAAAO");
			this.delete();
			System.exit(0); //provisional
		}
	}


	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}

}
