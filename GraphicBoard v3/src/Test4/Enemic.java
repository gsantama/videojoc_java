package Test4;


import Core.Field;
import Core.PhysicBody;
import Core.Sprite;


public class Enemic extends PhysicBody implements Constants{
	public Enemic(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.setVelocity(0, 1 );
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		if (sprite instanceof Nau) {
			sprite.delete();
			this.delete();
			System.out.println("MHAN MATAO");
			System.exit(0);	//provisional
		}		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
	}

	@Override
	public void onCollisionStay(Sprite sprite) {		
	}
}
